package cl.ubb.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Reserva{
	
	@Id
	@GeneratedValue
	private long id;
	private Date fechaInicio;
	private Date fechaFin;
	private long idAutomovil;
	private long idCliente;
	
	public Reserva(){
		
	}

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public long getIdAtomovil(){
		return idAutomovil;
	}
	
	public void setIdAutomovil(long idAutomovil){
		this.idAutomovil = idAutomovil;
	}

	public long getIdCliente(){
		return idCliente;
	}
	
	public void setIdCliente(long idCliente){
		this.idCliente = idCliente;
	}
}