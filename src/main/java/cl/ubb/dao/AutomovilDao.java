package cl.ubb.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Automovil;
import cl.ubb.model.Cliente;

public interface AutomovilDao extends CrudRepository<Automovil,Long> {
	@Query("select t from ToDo t where categoria = ?1")
	public ArrayList<Automovil> findByCategoria(String marca, String modelo, String tipoTransmicion);
}
