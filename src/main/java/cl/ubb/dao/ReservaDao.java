package cl.ubb.dao;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;
import cl.ubb.model.Reserva;

public interface ReservaDao extends CrudRepository<Reserva,Long> {
	public Cliente findById(Long id);

	public ArrayList<Reserva> findByReserva(long idCliente, long idAuto, Date fechaInit, Date fechaEnd);

	public ArrayList<Reserva> findByReservaCliente(long idAuto, Date fechaInit, Date fechaEnd);
}